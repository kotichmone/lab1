﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab1
{
    class Note
    {
        public static ArrayList notes = new ArrayList(); //Коллекция, которая хранит все объекты типа Note 
        public static int id = 0;
        private int idnote;
        private string Surname;
        private string Name;
        private string Otch;
        private long numer;
        private string Country;
        private DateTime birth;
        private string organisation;
        private string job;
        private string prochie;

        static void Main(string[] args)
        {

            Console.WriteLine("Привет, я записная книжка!");

            int vvod = 0;
            while (vvod != 7) //Значение ввод ==7 ведет к завершению программы
            {
                Console.WriteLine("1. Если хочешь создать новую запись, введи 1");
                Console.WriteLine("2. Если хочешь редактировать запись, введи 2");
                Console.WriteLine("3. Если хочешь удалить запись, введи 3");
                Console.WriteLine("4. Если хочешь посмотреть все записи, введи 4");
                Console.WriteLine("5. Если хочешь увидеть конкретную запись, введи 5");
                Console.WriteLine("6. Для выхода введи 6");
                while (vvod < 1)
                {
                    string s = Console.ReadLine();

                    if (int.TryParse(s, out vvod) != true) //значение ввод меньше 1 в случае если введенные пользователем данные не содержат чисел
                    {
                        Console.Clear();
                        Console.WriteLine("1. Если хочешь создать новую запись, введи 1");
                        Console.WriteLine("2. Если хочешь редактировать запись, введи 2");
                        Console.WriteLine("3. Если хочешь удалить запись, введи 3");
                        Console.WriteLine("4. Если хочешь посмотреть все записи, введи 4");
                        Console.WriteLine("5. Если хочешь увидеть конкретную запись, введи 5");
                        Console.WriteLine("6. Для выхода введи 6");
                        Console.WriteLine("Вы ввели неверное значение. Введите число от 1 до 6");

                    }

                }
                switch (vvod) //в случае, если вользователь ввел число в необходимом диапазоне, начинает выполняться данный switch 
                {
                    case 1:
                        New(); //метод создания новой записи

                        break;
                    case 2:
                        {
                            Console.WriteLine("Введите номер корректируемой записи ");

                            int i = -1;
                            string s = Console.ReadLine();
                            while (i < 0)
                            {

                                if (int.TryParse(s, out i) == true) //проверка типа введенных данных
                                {
                                    i = Convert.ToInt32(s);
                                    Correct(i);
                                }
                                else
                                {
                                    Console.WriteLine("Вы ввели неверное значение. Введите число");
                                    s = Console.ReadLine();
                                    i = -1; //флаг принимает начальное значене для продолжения цикла
                                }
                            }

                            Console.ReadKey();
                        }
                        break;
                    case 3:
                        {
                            Console.WriteLine("Введите номер удаляемой записи.\nЕсли вы не хотите ничего удалять введите \"нет\"");

                            int i = -1;
                            string s = Console.ReadLine();
                            while ((i < 0) && (s != "нет")) //проверка введенных данных 
                            {
                                if (int.TryParse(s, out i) == true)
                                {
                                    i = Convert.ToInt32(s);
                                    Delete(i);
                                }
                                else
                                {
                                    Console.WriteLine("Вы ввели неверное значение. Введите число");
                                    s = Console.ReadLine();
                                    i = -1;
                                }
                            }

                            Console.ReadKey();
                        }
                        break;

                    case 4:
                        All(); // метод вывода всех записей
                        break;
                    case 5:
                        {
                            Console.Clear();
                            Console.WriteLine("Введите номер записи для вывода. В базе "+notes.Capacity+" записей.");
                            string num = "";
                            int number = -2;
                            while (number < -1)
                            {
                                num = Console.ReadLine();
                                if (num == "") //проверка на пустое поле 
                                { Console.WriteLine("Введите номер . Поле не должно быть пустым"); }
                                else if (int.TryParse(num, out number))  //проверка на ввод чисел 
                                {
                                    See(number);
                                }
                                else
                                {
                                    Console.WriteLine("Номер не может содержать символы кроме цифр");
                                    number = -2;  //флаг принимает начальное значене для продолжения цикла
                                }
                            }
                            Console.ReadKey();

                        }
                        break;
                    case 6:
                        {
                            Console.WriteLine("Ты уверен, что хочешь выйти? введи да или нет");  //условие для уточнения намерений пользователя
                            string ans = Console.ReadLine();

                            while ((vvod != 7) && (ans != "нет"))
                            {
                                if (ans == "да")
                                {
                                    vvod = 7;
                                }


                                else
                                {
                                    Console.Clear();

                                    Console.WriteLine("Введи да или нет");
                                    ans = Console.ReadLine();
                                }
                            }
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Вы ввели неверное значение. Введите число от 1 до 6"); //заданное условие выполняется, если пользователь ввел число непринадлежащее нужному диапазону
                            Console.ReadKey();
                            string value = Console.ReadLine();
                            int i = -1;
                            if (int.TryParse(value, out i))
                                i = Convert.ToInt32(value);
                        }
                        break;

                }

                Console.Clear();
                if (vvod != 7)
                { vvod = 0; }
            }

            Console.WriteLine("Bye! ");
            Console.ReadKey();
        }

        static void New()
        {
            Console.Clear();
            Console.WriteLine("Введите имя");
            string name = "";
            while (name == "") //проверка на заполнение значения
            {
                name = Console.ReadLine();
                if (name == "")
                { Console.WriteLine("Введите имя. Поле не должно быть пустым"); }
            }

            Console.Clear();
            Console.WriteLine("Введите фамилию");
            string surname = "";
            while (surname == "")
            {
                surname = Console.ReadLine();
                if (surname == "")
                { Console.WriteLine("Введите фамилию . Поле не должно быть пустым"); }
            }

            Console.Clear();
            Console.WriteLine("Введите отчество");
            string otch = "";
            while (otch == "")
            {
                otch = Console.ReadLine();
                if (otch == "")
                { Console.WriteLine("Введите отчество . Поле не должно быть пустым"); }
            }

            Console.Clear();
            Console.WriteLine("Введите номер");
            string num = "";
            long number = 0;
            while (number < 10000)
            {
                num = Console.ReadLine();
                if (num == "")
                { Console.WriteLine("Введите номер . Поле не должно быть пустым"); }
                else if (long.TryParse(num, out number))
                {
                    if (number < 10000)
                    { Console.WriteLine("Номер не может быть слишком коротким"); }

                }
                else
                {
                    Console.WriteLine("Номер не может содержать символы кроме цифр или быть слишком длинным");
                }
            }

            Console.Clear();
            Console.WriteLine("Введите страну");
            string country = "";
            while (country == "")
            {
                country = Console.ReadLine();
                if (country == "")
                { Console.WriteLine("Введите страну . Поле не должно быть пустым"); }
            }
            Console.Clear();
            DateTime birth;
            string org;
            string job;
            string prochie;
            Console.WriteLine("Есть ли иная информация? Если да, введите да. Если нет, любую клавишу"); //необязательная информация 
            if (Console.ReadLine() == "да")
            {
                var usCulture = new System.Globalization.CultureInfo("en-US");
                Console.WriteLine("Введите дату рождения в формате " + usCulture.DateTimeFormat.ShortDatePattern);
                string date = Console.ReadLine();
                try
                {
                    birth = DateTime.Parse(date, usCulture.DateTimeFormat); //ввод даты
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Ошибка ввода даты {ex}");
                    birth = DateTime.Parse("01/01/1970"); //Если пользователь ввел дату не в указанном формате, значение переменной даты будет 01.01.1970
                }


                Console.Clear();
                Console.WriteLine("Введите название организации");
                org = Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Введите должность");
                job = Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Введите прочую информацию");
                prochie = Console.ReadLine();

            }
            else //в случае, если пользователь указал, что дополнительной информации нет, переменные принимают следующие значения
            {
                birth = DateTime.Parse("01/01/1970");
                org = "";
                job = "";
                prochie = "";
            }
            Note note = new Note(name, surname, otch, number, country, birth, org, job, prochie);
            notes.Add(note); //добавление созданной записи с необходимыми параметрами
            Console.Clear();
            Console.WriteLine("Добавлено");
            Console.ReadKey();
        }
        static void Correct(int i)
        {
            if (notes.Count - 1 < i)
            {
                Console.WriteLine("Такой записи не существует ");
            }
            else
            {
                foreach (Note t in notes)
                {
                    if (t.idnote == i)
                    { Console.WriteLine(t); }
                }
                string valuecorrect = "value"; //переменная, означающая название параметров
                while (valuecorrect != "")
                {
                    Console.Clear();
                    valuecorrect = "value";
                    foreach (Note t in notes)
                    {
                        if (t.idnote == i)
                        { Console.WriteLine(t); }
                    }
                    Console.WriteLine("Введите название одного поля, которое вы хотите изменить в записи " + i);
                    Console.WriteLine("Фамилия, Имя, Отчество, Номер, Страна, ");
                    Console.WriteLine("Дата рождения, Организация, Должность, Прочая информация ");
                    Console.WriteLine("Для выхода нажмите Enter");
                    valuecorrect = Console.ReadLine();
                    string valueparametr = ""; //переменная, означающая изменяемое значение заданного параметра

                    switch (valuecorrect)
                    {
                        case "Фамилия":
                            {
                                Console.WriteLine("Введите фамилию");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.Surname = valueparametr;
                                        Console.WriteLine("Фамилия изменена");
                                        Console.ReadKey();
                                    }
                                }


                            }
                            break;
                        case "Имя":
                            {
                                Console.WriteLine("Введите имя");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.Name = valueparametr;
                                        Console.WriteLine("Имя изменено");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "Отчество ":
                            {
                                Console.WriteLine("Введите имя");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.Otch = valueparametr;
                                        Console.WriteLine("Отчество изменено");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "Номер":
                            {

                                Console.WriteLine("Введите номер");
                                string num = "";
                                int number = 0;
                                while (number < 10000)
                                {
                                    num = Console.ReadLine();
                                    if (num == "")
                                    { Console.WriteLine("Введите номер . Поле не должно быть пустым"); }
                                    else if (int.TryParse(num, out number))
                                    {
                                        if (number < 10000)
                                        { Console.WriteLine("Номер не может быть слишком коротким"); }
                                        else
                                        {
                                            foreach (Note note in notes)
                                            {
                                                if (note.idnote == i)
                                                {
                                                    note.numer = number;
                                                    Console.WriteLine("Номер изменен");
                                                    Console.ReadKey();
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        Console.WriteLine("Номер не может содержать символы кроме цифр или быть слишком длинным");
                                    }
                                }
                            }
                            break;
                        case "Страна":
                            {
                                Console.WriteLine("Введите страну");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.Country = valueparametr;
                                        Console.WriteLine("Страна изменена");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "Организация":
                            {
                                Console.WriteLine("Введите организацию");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.organisation = valueparametr;
                                        Console.WriteLine("Организация изменена");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "Должность":
                            {
                                Console.WriteLine("Введите должность");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.job = valueparametr;
                                        Console.WriteLine("Должность изменена");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "Дата рождения":
                            {
                                var usCulture = new System.Globalization.CultureInfo("en-US");
                                Console.WriteLine("Введите дату рождения в формате " + usCulture.DateTimeFormat.ShortDatePattern);
                                string date = Console.ReadLine();

                                try
                                {
                                    foreach (Note note in notes)
                                    {
                                        if (note.idnote == i)
                                        {
                                            note.birth = DateTime.Parse(date, usCulture.DateTimeFormat);
                                            Console.WriteLine("Дата изменена");
                                            Console.ReadKey();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine($"Ошибка ввода даты {ex}");
                                    foreach (Note note in notes)
                                    {
                                        if (note.idnote == i)
                                        {
                                            note.birth = DateTime.MinValue;
                                            Console.WriteLine("Дата изменена на минимальную");
                                            Console.ReadKey();
                                        }
                                    }
                                }





                            }
                            break;
                        case "Прочая информация":
                            {
                                Console.WriteLine("Введите информацию");
                                valueparametr = Console.ReadLine();
                                foreach (Note note in notes)
                                {
                                    if (note.idnote == i)
                                    {
                                        note.prochie = valueparametr;
                                        Console.WriteLine("Прочая информация изменена");
                                        Console.ReadKey();
                                    }
                                }
                            }
                            break;
                        case "":
                            {
                                Console.WriteLine("Выход");  //заданное условие выполняется, в случае если пользователь нажал Enter 
                                Console.ReadKey();
                            }
                            break;
                        default:
                            {
                                Console.WriteLine("Таких типов данных нет либо нет записи с таким номером");
                                Console.ReadKey();
                            }
                            break;
                    }


                }
            }
        }


        static void All()
        {
            if (notes.Capacity == 0)
            {
                Console.Clear();
                Console.WriteLine("В записной книжке пока нет данных. Для продолжения нажмите любую клавишу");


            }
            foreach (Note t in notes)
            {
                Console.WriteLine($"Имя               {t.Name} \nФамилия           {t.Surname}\nНомер             {t.numer}\n\n");
            }
            Console.ReadKey();
        }
        static void See(int i)
        {
            if ((notes.Capacity == 0) || (notes.Capacity < i)) //проверка, ввел ли пользователь запрашиваемую запись с несуществующим id и на пустую коллекцию note 
            {
                Console.WriteLine("В записной книжке нет таких данных. Для продолжения нажмите любую клавишу");
            }
            foreach (Note t in notes)
            {
                if (t.idnote == i)
                { Console.WriteLine(t); }
            }
        }

        public override string ToString()
        {
            return $"Id                {this.idnote} \nИмя               {this.Name} \nФамилия           {this.Surname} \nОтчество          {this.Otch} \nНомер             {this.numer} \nСтрана            {this.Country} \nДата рождения     {this.birth} \nОрганизация       {this.organisation} \nДолжность         {this.job} \nПрочая информация {this.prochie} \n \n ";
        }

        static void Delete(int i)
        {
            if (notes.Count > i) //проверка 
            {
                notes.RemoveAt(i);
                Console.WriteLine("Запись удалена");
            }
            else
            {
                Console.WriteLine("Такой записи не существует ");

            }
        }
        public Note(string surname, string name, string otch, long numer, string country, DateTime birth, string organisation, string job, string prochie)
        {
            this.idnote = id;
            id++;
            Surname = surname;
            Name = name;
            Otch = otch;
            this.numer = numer;
            Country = country;
            this.birth = birth;
            this.organisation = organisation;
            this.job = job;
            this.prochie = prochie;
        }
    }
}
